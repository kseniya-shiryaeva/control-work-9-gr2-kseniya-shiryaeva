<?php

namespace App\Repository;

use App\Entity\User;
use App\Model\User\UserHandler;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    /**
     * @var UserHandler
     */
    protected $userHandler;

    public function __construct(
        RegistryInterface $registry,
        UserHandler $userHandler)
    {
        parent::__construct($registry, User::class);
        $this->userHandler = $userHandler;
    }

    /**
     * @param string $plainPassword
     * @param string $email
     * @return User|null
     * @throws \App\Model\Api\ApiException
     */
    public function getByCredentials(string $plainPassword, string $email)
    {

        try {
            return $this->createQueryBuilder('a')
                ->select('a')
                ->where('a.password = :password')
                ->andWhere('a.email = :email')
                ->setParameter(
                    'password',
                    $this->userHandler->encodePlainPassword($plainPassword)
                )
                ->setParameter('email', $email)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    /**
     * @param string $email
     * @return User|null
     */
    public function getByEmail(string $email)
    {

        try {
            return $this->createQueryBuilder('a')
                ->select('a')
                ->where('a.email = :email')
                ->setParameter('email', $email)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    public function getByNetwork(string $network, string $social_id){
        try {
            return $this->createQueryBuilder('a')
                ->select('a')
                ->where(':network = :social_id')
                ->setParameter(
                    'network', 'a.'.$network
                )
                ->setParameter(
                    'social_id', $social_id
                )
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }
}
