<?php

namespace App\Repository;

use App\Entity\Follow;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Follow|null find($id, $lockMode = null, $lockVersion = null)
 * @method Follow|null findOneBy(array $criteria, array $orderBy = null)
 * @method Follow[]    findAll()
 * @method Follow[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FollowRepository extends ServiceEntityRepository
{

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Follow::class);
    }

    /**
     * @param string $name
     * @param User $user
     * @return mixed|null
     */
    public function getByFollowName(string $name, User $user)
    {

        try {
            return $this->createQueryBuilder('a')
                ->select('a')
                ->where('a.item = :item')
                ->andWhere('a.user = :user')
                ->setParameter('item', $name)
                ->setParameter('user', $user)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    /**
     * @param string $name
     * @return mixed|null
     */
    public function getFollowCount(string $name)
    {

        try {
            return $this->createQueryBuilder('a')
                ->select('a')
                ->where('a.item = :item')
                ->setParameter('item', $name)
                ->getQuery()
                ->getResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    /**
     * @param User $user
     * @param int $offset
     * @param int $limit
     * @return mixed|null
     */
    public function getNamesByUser(User $user, int $offset, int $limit)
    {

        try {
            return $this->createQueryBuilder('a')
                ->select('a.item')
                ->where('a.user = :user')
                ->setParameter('user', $user)
                ->addOrderBy('a.createDate', 'DESC')
                ->setFirstResult( $offset )
                ->setMaxResults( $limit )
                ->getQuery()
                ->getResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    /**
     * @param User $user
     * @return mixed|null
     */
    public function getCountNamesByUser(User $user)
    {

        try {
            return $this->createQueryBuilder('a')
                ->select('COUNT(a.item) as item_count')
                ->where('a.user = :user')
                ->setParameter('user', $user)
                ->getQuery()
                ->getResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }
}
