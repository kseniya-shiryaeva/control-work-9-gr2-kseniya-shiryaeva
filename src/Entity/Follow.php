<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class Follow
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="follows")
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=64)
     */
    private $item;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $createDate;

    public function __construct()
    {
        $this->createDate = (date("Y-m-d H:i:s", time()));
    }

    /**
     * @param User $user
     * @return Follow
     */
    public function setUser(User $user): Follow
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param string $item
     * @return Follow
     */
    public function setItem(string $item): Follow
    {
        $this->item = $item;
        return $this;
    }

    /**
     * @return string
     */
    public function getItem(): string
    {
        return $this->item;
    }

    /**
     * @param string $createDate
     * @return Follow
     */
    public function setCreateDate(string $createDate): Follow
    {
        $this->createDate = $createDate;
        return $this;
    }
}
