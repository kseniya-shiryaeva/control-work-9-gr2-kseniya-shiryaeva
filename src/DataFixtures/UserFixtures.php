<?php
namespace App\DataFixtures;

use App\Model\User\UserHandler;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    /**
     * @var UserHandler
     */
    private $userHandler;

    public function __construct(UserHandler $userHandler)
    {
        $this->userHandler = $userHandler;
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {

        $user1 = $this->userHandler->createNewUser([
            'email' => 'kkk@kkk.ru',
            'passport' => 'passport 111',
            'password' => '123',
            'roles' => ['ROLE_USER']
        ], true);

        $manager->persist($user1);
        $this->addReference('user1', $user1);

        $user2 = $this->userHandler->createNewUser([
            'email' => 'ewq@qwe.ru',
            'passport' => 'qwerty & some',
            'password' => '123',
            'roles' => ['ROLE_USER']
        ], true);

        $manager->persist($user2);
        $this->addReference('user2', $user2);

        $user3 = $this->userHandler->createNewUser([
            'email' => 'ddd@ddd.ru',
            'passport' => 'qddd',
            'password' => '123',
            'roles' => ['ROLE_USER']
        ], true);

        $manager->persist($user3);
        $this->addReference('user3', $user3);

        $manager->flush();
    }
}
