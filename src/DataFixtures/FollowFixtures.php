<?php
namespace App\DataFixtures;

use App\Entity\Follow;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class FollowFixtures extends Fixture implements DependentFixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {

        $user1 = $this->getReference('user1');
        $user2 = $this->getReference('user2');
        $user3 = $this->getReference('user3');

        $follow1 = new Follow();

        $follow1->setItem('t3_8wgone')
            ->setUser($user1)
            ->setCreateDate('2018-07-07 11:05:34');

        $manager->persist($follow1);

        $follow2 = new Follow();

        $follow2->setItem('t3_8w9yre')
            ->setUser($user1)
            ->setCreateDate('2018-07-07 11:41:07');

        $manager->persist($follow2);

        $follow3 = new Follow();

        $follow3->setItem('t3_8sqpki')
            ->setUser($user1)
            ->setCreateDate('2018-07-07 13:51:58');

        $manager->persist($follow3);

        $follow4 = new Follow();

        $follow4->setItem('t3_8qep03')
            ->setUser($user1)
            ->setCreateDate('2018-07-07 13:52:20');

        $manager->persist($follow4);

        $follow5 = new Follow();

        $follow5->setItem('t3_8widua')
            ->setUser($user1)
            ->setCreateDate('2018-07-07 13:52:33');

        $manager->persist($follow5);



        $follow6 = new Follow();

        $follow6->setItem('t3_8sqpki')
            ->setUser($user2)
            ->setCreateDate('2018-07-07 13:51:58');

        $manager->persist($follow6);

        $follow7 = new Follow();

        $follow7->setItem('t3_8qep03')
            ->setUser($user2)
            ->setCreateDate('2018-07-07 13:52:20');

        $manager->persist($follow7);

        $follow8 = new Follow();

        $follow8->setItem('t3_8widua')
            ->setUser($user2)
            ->setCreateDate('2018-07-07 13:52:33');

        $manager->persist($follow8);

        $follow9 = new Follow();

        $follow9->setItem('t3_8qaaoq')
            ->setUser($user1)
            ->setCreateDate('2018-07-07 13:52:33');

        $manager->persist($follow9);

        $manager->flush();
    }

    public function getDependencies()

    {

        return array(

            UserFixtures::class,

        );

    }
}
