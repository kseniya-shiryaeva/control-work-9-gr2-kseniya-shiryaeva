<?php

namespace App\Model\Api;


use Curl\Curl;
use Symfony\Bundle\FrameworkBundle\Tests\CacheWarmer\testRouterInterfaceWithoutWarmebleInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ApiContext extends AbstractApiContext
{
    const ENDPOINT_FILTER = '/r/picture/search.json';
    const ENDPOINT_ITEM = '/api/info.json';
    const ENDPOINT_FAVORITES = '/api/info.json';

    /**
     * @param array $parameters
     * @return mixed
     * @throws ApiException
     */
    public function makeRequest(array $parameters)
    {
        return $this->makeQuery(self::ENDPOINT_FILTER, self::METHOD_GET, ['q' => $parameters['q'],'sort' => $parameters['sort'],'limit' => $parameters['limit'],'type'=>'link']);
    }

    /**
     * @param string $parameter
     * @return mixed
     * @throws ApiException
     */
    public function getItem(string $parameter)
    {
        return $this->makeQuery(self::ENDPOINT_ITEM, self::METHOD_GET, ['id' => $parameter]);
    }

    /**
     * @param string $parameter
     * @return mixed
     * @throws ApiException
     */
    public function getFavorites(string $parameter)
    {
        return $this->makeQuery(self::ENDPOINT_FAVORITES, self::METHOD_GET, ['id' => $parameter]);
    }
}