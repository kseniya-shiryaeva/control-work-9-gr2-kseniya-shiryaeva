<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class FilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'q',
                TextType::class, [
                    'label' => ' ',
                ]
            )
            ->add('sort', ChoiceType::class, [
                'label' => ' ',
                'choices' => [
                    'hot' => 'hot',
                    'relevance' => 'relevance',
                    'top' => 'top',
                    'new' => 'new',
                    'comments' => 'comments'
                ],
                'data' => 'hot'
            ])
            ->add('limit', ChoiceType::class, [
                'label' => ' ',
                'choices' => [
                    'How many?' => null,
                    '8 items' => 8,
                    '16 items' => 16,
                    '24 items' => 24,
                    '32 items' => 32
                ]
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Искать'
            ]);

        ;
    }
}
