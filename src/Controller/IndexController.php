<?php

namespace App\Controller;

use App\Entity\User;
use App\Model\User\UserHandler;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends Controller
{

    /**
     * @Route("/sign-up", name="app-sign-up")
     *
     * @param Request $request
     * @param ObjectManager $manager
     * @param UserHandler $userHandler
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function signUpAction(
        Request $request,
        ObjectManager $manager,
        UserHandler $userHandler
    )
    {
        $error = null;
        $user = new User();
        $form = $this->createForm("App\Form\RegisterType", $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = [
                'email' => $user->getEmail(),
                'passport' => $user->getPassport(),
                'password' => $user->getPassword(),
                'roles' => $user->getRoles(),
            ];

            $user = $userHandler->createNewUser($data);

            if($user){
                $manager->persist($user);
                $manager->flush();

                return $this->redirectToRoute("auth");
            }
        }

        return $this->render('sign_up.html.twig', [
            'form' => $form->createView(),
            'error' => $error
        ]);
    }

    /**
     * @Route("/auth", name="auth")
     *
     * @param UserRepository $userRepository
     * @param Request $request
     * @param UserHandler $userHandler
     * @param ObjectManager $manager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     * @throws \App\Model\Api\ApiException
     */
    public function authAction(
        UserRepository $userRepository,
        Request $request,
        UserHandler $userHandler,
        ObjectManager $manager
    )
    {
        $error = null;

        $form = $this->createForm("App\Form\AuthType");

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            $user = $userRepository->getByCredentials(
                $data['password'],
                $data['email']
            );

            if ($user) {
                $userHandler->makeUserSession($user);
                return $this->redirectToRoute('start_page');
            }
        }

        return $this->render(
            '/login.html.twig', [
            'form' => $form->createView(),
            'error' => $error
        ]);
    }

    /**
     * @Route("/", name="home")
     *
     * @return Response
     */
    public function indexAction()
    {
        return $this->render('index.html.twig', []);

    }

    /**
     * @Route("/cabinet", name="client_cabinet")
     */
    public function showAction()
    {

        $user = $this->getUser();

        $providers = [];

        if($user->getFaceBookId() == null){
            $providers[] = 'facebook';
        }

        if($user->getVkId() == null){
            $providers[] = 'vkontakte';
        }

        if($user->getGoogleId() == null){
            $providers[] = 'google';
        }

        if(count($providers) > 0){
            $providers = implode(',', $providers);
        }

        return $this->render('cabinet.html.twig', [
            'user' => $this->getUser(),
            'providers' => $providers
        ]);

    }

    /**
     * @Route("/start_page", name="start_page")
     *
     * @param Request $request
     * @return Response
     */
    public function startWorkAction(Request $request)
    {
        $user = $this->getUser();
        $user_type = $user->getRoles();

        if(in_array('ROLE_USER', $user_type)){
            return $this->redirectToRoute('catalog');
        }

        return $this->render('start_page.html.twig', [
            'user' => $this->getUser()
        ]);
    }
}
