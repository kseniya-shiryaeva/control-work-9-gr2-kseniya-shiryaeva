<?php
/**
 * Created by PhpStorm.
 * User: kseniya
 * Date: 07.07.18
 * Time: 13:33
 */

namespace App\Controller;


use App\Entity\Follow;
use App\Model\Api\ApiContext;
use App\Repository\FollowRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CatalogController extends Controller
{

    /**
     * @Route("/catalog", name="catalog")
     *
     * @param ApiContext $apiContext
     * @param Request $request
     * @param FollowRepository $followRepository
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \App\Model\Api\ApiException
     */
    public function indexAction(ApiContext $apiContext, Request $request, FollowRepository $followRepository)
    {

        $form = $this->createForm('App\Form\FilterType');

        $form->handleRequest($request);

        $parameters = [
            'q' => 'cat',
            'sort' => 'hot',
            'limit' => '8'
        ];

        if($form->isSubmitted()){
            $filterData = $form->getData();

            if($filterData['q']){
                $parameters['q'] = $filterData['q'];
            }

            if($filterData['sort']){
                $parameters['sort'] = $filterData['sort'];
            }

            if($filterData['limit']){
                $parameters['limit'] = $filterData['limit'];
            }
        }

        $reddits = $apiContext->makeRequest($parameters);

        $data = [];
        $i = 0;

        foreach ($reddits['data']['children'] as $reddit){
            $item = $reddit['data'];

            $data[$i]['name'] = $item['name'] ?? '';
            $data[$i]['thumbnail'] = $item['thumbnail'] ?? '';
            $data[$i]['title'] = $item['title'] ?? '';
            $data[$i]['author'] = $item['author'] ?? '';
            $data[$i]['create'] = date('d/m/Y',$item['created_utc']) ?? '';

            $data[$i]['followersCount'] = count($followRepository->getFollowCount($item['name']));

            $i++;
        }

        return $this->render('catalog.html.twig', [
            'form' => $form->createView(),
            'data' => $data,
            'user' => $this->getUser()
        ]);
    }


    /**
     * @Route("/catalog_item/{name}", name="catalog_item")
     *
     * @param string $name
     * @param ApiContext $apiContext
     * @param FollowRepository $followRepository
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \App\Model\Api\ApiException
     */
    public function showItemAction(string $name, ApiContext $apiContext, FollowRepository $followRepository){


        $reddit = $apiContext->getItem($name);

        $item = $reddit['data']['children']['0']['data'];

        $data['name'] = $name;
        $data['image'] = $item['url'] ?? '';
        $data['title'] = $item['title'] ?? '';
        $data['author'] = $item['author'] ?? '';
        $data['create'] = date('d/m/Y',$item['created_utc']) ?? '';

        $followersCount = count($followRepository->getFollowCount($name));

        $button = $followRepository->getByFollowName($name, $this->getUser());

        return $this->render('catalog_item.html.twig', [
            'data' => $data,
            'user' => $this->getUser(),
            'button' => $button,
            'followersCount' => $followersCount
        ]);
    }

    /**
     * @Route("/set_follow/{name}", name="set_follow")
     *
     * @param string $name
     * @param ObjectManager $manager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function setFollowAction(string $name, ObjectManager $manager)
    {
        if($this->getUser()){
            $follow = new Follow();

            $follow->setUser($this->getUser());
            $follow->setItem($name);

            $manager->persist($follow);
            $manager->flush();

            return $this->redirectToRoute("catalog_item", [
                'name' => $name
            ]);
        }

        return $this->redirectToRoute("auth");
    }

    /**
     * @Route("/my_favorites/{page}", name="my_favorites")
     * @param int $page
     * @param ApiContext $apiContext
     * @param FollowRepository $followRepository
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \App\Model\Api\ApiException
     */
    public function myFavoritesAction(int $page, ApiContext $apiContext, FollowRepository $followRepository)
    {
        $parameters = [
            'q' => 'cat',
            'sort' => 'hot',
            'limit' => 4
        ];

        $offset = $parameters['limit']*($page-1)+1;
        $limit = $parameters['limit'];

        $countFollowsArray = $followRepository->getCountNamesByUser($this->getUser());
        $countFollows = $countFollowsArray[0]['item_count'];

        if($countFollows > $limit){
            $pages = [];
            for($i=1; $i <= ceil($countFollows/$limit); $i++){
                $pages[] = $i;
            }
        }

        $follows = $followRepository->getNamesByUser($this->getUser(), $offset, $limit);

        $parameter = '';

        foreach ($follows as $follow){
            $parameters[] = $follow['item'];
        }

        $parameter = implode(',', $parameters);

        if($parameter !== ''){
            $reddits = $apiContext->getFavorites($parameter);

            $data = [];
            $i = 0;

            foreach ($reddits['data']['children'] as $reddit){
                $item = $reddit['data'];

                $data[$i]['name'] = $item['name'] ?? '';
                $data[$i]['thumbnail'] = $item['thumbnail'] ?? '';
                $data[$i]['title'] = $item['title'] ?? '';
                $data[$i]['author'] = $item['author'] ?? '';
                $data[$i]['create'] = date('d/m/Y',$item['created_utc']) ?? '';

                $data[$i]['followersCount'] = count($followRepository->getFollowCount($item['name']));

                $i++;
            }
        }



        return $this->render('favorites.html.twig', [
            'data' => $data ?? null,
            'user' => $this->getUser(),
            'pages' => $pages ?? null
        ]);
    }
}